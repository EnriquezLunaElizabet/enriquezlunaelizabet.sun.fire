<?php
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));
$app->container->singleton('db', function () {  return new 
PDO("pgsql:host=127.0.0.1 user=app_usuario password=12345 dbname=a10160833");
});

require '../../app/routes/root.php';
require '../../app/routes/xml.php';

$app->run();

